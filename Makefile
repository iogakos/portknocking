CC = gcc

NETLABCLIENT = client
NETLABSERVER = server
NETLABLIBSRV = initUDPsocket initTCPsocket validateUDPknocking validateTCPknocking
NETLABLIB = libnetlab

SRCDIR = $(PWD)/src/
BINDIR = $(PWD)/bin/
LIBDIR = $(PWD)/lib/

LIBSRCDIR = $(SRCDIR)libsrc/
INCDIR = $(SRCDIR)include/

OBJS = initUDPsocket.o initTCPsocket.o validateUDPknocking.o validateTCPknocking.o

DEBUG = -g
CFLAGS = -I $(INCDIR) -Wall -Wextra -O2 -c 
LDFLAGS = $(DEBUG)

library : $(LIBSRCDIR)
		$(foreach netlab_service, $(NETLABLIBSRV), $(CC) -c -o $(LIBDIR)$(netlab_service).o $(LIBSRCDIR)$(netlab_service).c $(CFLAGS);)
		ar qv $(LIBDIR)$(NETLABLIB).a $(LIBDIR)*.o
		ranlib $(LIBDIR)$(NETLABLIB).a

client: $(UTILSRCDIR)
		$(CC) -o $(BINDIR)$(NETLABCLIENT)  $(SRCDIR)$(NETLABCLIENT).c -I $(INCDIR) -Wall -Wextra $(LDFLAGS) -lnetlab -L$(LIBDIR)

server: $(UTILSRCDIR)
		$(CC) -o $(BINDIR)$(NETLABSERVER)  $(SRCDIR)$(NETLABSERVER).c -I $(INCDIR) -Wall -Wextra $(LDFLAGS) -lnetlab -L$(LIBDIR)

all:
	make clean
	mkdir $(LIBDIR)
	mkdir $(BINDIR)
	make library
	make client
	make server

clean:
	rm -rf $(LIBDIR)
	rm -rf $(BINDIR)
