#include <stdio.h>		/* Standard input/output library		*/
#include <stdlib.h>		/* General purpose standard library 	*/
#include <string.h>
#include <unistd.h>		/*	prototypes for the string and buffer*
 						 * 				manipulation routines 	*/
#include <sys/socket.h> /* Socket library (socklen_t)			*/
#include <arpa/inet.h> 	/* Definitions for internet operations	*/
#include <errno.h>		/* Handling errors						*/

#include "macros.h"
#include "defs.h"
#include "netlab.h"

int main(int argc, char *argv[]){

	int i, udpsd, tcpsd;
	struct sockaddr_in *UDPserver, *TCPserver;
	short int udp_port[AVAILUDPPORTS], tcp_port;
	char *host;
	
	host = argv[1];

	/* Handle server's execution line. MIN/MAXARGS	*
	 * actually represent minimum and maximum 		*
	 * number of ports the server can support.		*/
	if( argc < MINARGS_CLIENT || argc > MAXARGS ){
		printf("[NUM OF ARGS]: %d\n", argc);
		printf("Client: ./client ...\n");
		exit(1);
	}

	if(!strcmp(argv[2],"tcp")){
		/* If client has passed the UDP port 		*
		 * knocking sequence and the only port to 	*
		 * authenticate is the TCP port then he		*
		 * executes: ./bin/client <host> tcp <port>	*/
		tcp_port = atoi(argv[argc-1]);
		TCPserver = initTCPsocket_client( tcp_port, &tcpsd);
		validateTCPknocking_client(tcpsd);
		return OK;
	}
	
	/* Parse from shell the ports server gave. The	*
	 * last port is the port will be used for the 	*
	 * tcp knocking									*/	
	for( i = 0; i < argc - 2; i++)
		udp_port[i] = atoi(argv[i+2]);

	tcp_port = atoi(argv[argc-1]);
	
	socklen_t slen=sizeof(struct sockaddr_in );
	printf("Port knocking just begun\n");
	
	for(i = 0; i< argc-3; i++){
		/* Loop through all ports the client gave 	*
		 * to execute the port knoncking using the 	*
		 * sequence he gave on execution time. Send	*
		 * a "ping" message to each corresponding 	*
		 * port and await for a "pong" response 	*
		 * from the server.							*/
		UDPserver = initUDPsocket_client(udp_port[i],&udpsd, host);
		validateUDPknocking_client(udpsd, udp_port[i], UDPserver, slen);
		memset(UDPserver, 0 , sizeof(struct sockaddr_in));
	}
	printf("UDP port knocking sequence successfully authenticated\n");
	close(udpsd);
	TCPserver = initTCPsocket_client( tcp_port, &tcpsd);
	validateTCPknocking_client(tcpsd);
	return OK;
}
