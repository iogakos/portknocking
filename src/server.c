#include <stdio.h>		/* Standard input/output library		*/
#include <stdlib.h>		/* General purpose standard library 	*/
#include <string.h>
#include <unistd.h>		/*	prototypes for the string and buffer*
 						 * 				manipulation routines 	*/
#include <sys/socket.h> /* Socket library (socklen_t)			*/
#include <arpa/inet.h> 	/* Definitions for internet operations	*/
#include <errno.h>		/* Handling errors						*/

#include "netlab.h"
#include "defs.h"
#include "macros.h"

int main(int argc, char *argv[]){
	
	int udpsd, tcpsd, i;
	struct sockaddr_in *TCPclient,*UDPclient;
	
	short int udp_port[AVAILUDPPORTS];
	short int tcp_port;
	
	/* Handle server's execution line. MIN/MAXARGS	*
	 * actually represent minimum and maximum		*
	 * number of ports the server can support.		*/
	if( argc < MINARGS_SERVER || argc > MAXARGS ){
		printf("[NUM OF ARGS]: %d\n", argc);
		printf("Server: ./server ...\n");
		exit(1);
	}
	
	for( i = 0; i < argc - 2; i++){
		/* Parse from shell the ports server gave. The	*
		 * last port is the port will be used for the	*
		 * tcp knocking									*/
		udp_port[i] = atoi(argv[i+1]);
	}
	
	/* The last port the user gave is actually the	*
	 * tcp port.									*/
	tcp_port = atoi(argv[argc-1]);
	socklen_t clen=sizeof(struct sockaddr_in );
	printf("Server is waiting for knocks...\n");
	
	for( i = 0; i < argc-2; i++){
		/* Loop through all ports the client gave	*
		 * to execute the port knoncking using the	*
		 * sequence he gave on execution time. Send *
		 * a "pong" message for each corresponding	*
		 * port to verify port knock on the specific*
		 * port										*/
		UDPclient = initUDPsocket_server(udp_port[i], &udpsd);
		validateUDPknocking_server(udpsd, udp_port[i], UDPclient, clen);
		memset(UDPclient,0, sizeof(struct sockaddr_in));
	}
	close(udpsd);
	/* Final step to validate port knocking: Client *
	 * knocked specific TCP port					*/
	TCPclient = initTCPsocket_server(tcp_port, &tcpsd);
	validateTCPknocking_server(tcpsd);
	close(tcpsd);
	return OK;
}
