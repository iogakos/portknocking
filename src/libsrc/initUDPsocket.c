#include <stdio.h>		/* Standard input/output library		*/
#include <stdlib.h>		/* General purpose standard library 	*/
#include <string.h>
#include <unistd.h>		/*	prototypes for the string and buffer*
 						 * 				manipulation routines 	*/
#include <sys/socket.h> /* Socket library (socklen_t)			*/
#include <arpa/inet.h> 	/* Definitions for internet operations	*/
#include <errno.h>		/* Handling errors						*/

#include <time.h>		/* Library handling time routines		*/

#include "macros.h"
#include "defs.h"
#include "netlab.h"

/*==================================================================*
 *							CLIENT									*
 *==================================================================*/
struct sockaddr_in * initUDPsocket_client(short int udp_port, int *udpsd, char *host){
	
	struct sockaddr_in *server;
	struct timeval to;
	
	server = malloc(sizeof(struct sockaddr_in ));
	memset(server, 0 , sizeof(struct sockaddr_in ));
	
	/* Create a new socket on the client side to connect to	*
	 * server												*/
	SOCKET_ON_ERROR_FAIL( *udpsd, AF_INET, SOCK_DGRAM, 0)
	
	to.tv_sec = 2 ;
	to.tv_usec	= 0 ;

	/* Set timeout range at 2 sec for the recvfrom routine. If		*
	 * client fails to receive response from server it just timeouts*/
	if(setsockopt(*udpsd, SOL_SOCKET, SO_RCVTIMEO, &to, sizeof(struct timeval))<0){
		perror("setsockopt");
	}
	
	/* Fill in servers' information to connect to					*/
	server->sin_family = AF_INET;
	server->sin_port = htons(udp_port);
	
	/* Convert the ip address from dotted decimal expression in 	*
	 * in_addr_t type to be correctly handled by the library		*/
	if ( inet_aton(host, &server->sin_addr ) == 0) {
		fprintf(stderr, "inet_aton() failed\n");
		perror("inet_aton()");
		exit(1);
	}
	return server;
}

/*==================================================================*
 *							SERVER									*
 *==================================================================*/
struct sockaddr_in * initUDPsocket_server(short int port, int *udpsd){

	struct sockaddr_in *client;
	
	client = malloc(sizeof(struct sockaddr_in));
	memset(client, 0, sizeof(struct sockaddr_in));
	
	/* Create a UDP socket							*/
	SOCKET_ON_ERROR_FAIL( *udpsd, AF_INET, SOCK_DGRAM, 0)

	socklen_t length = sizeof(struct sockaddr_in);
	
	client->sin_family = AF_INET;
	client->sin_addr.s_addr = htonl(INADDR_ANY);
	client->sin_port=htons(port);
	
	/*Desmeuse to port pou molis entopises oti einai eleuthero*/
	BIND_ON_ERROR_FAIL(*udpsd,(struct sockaddr*) client, length)
	return client;
}
