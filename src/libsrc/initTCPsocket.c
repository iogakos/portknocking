#include <stdio.h>		/* Standard input/output library		*/
#include <stdlib.h>		/* General purpose standard library 	*/
#include <string.h>
#include <unistd.h>		/*	prototypes for the string and buffer*
 						 * 				manipulation routines 	*/
#include <sys/socket.h> /* Socket library (socklen_t)			*/
#include <arpa/inet.h> 	/* Definitions for internet operations	*/
#include <errno.h>		/* Handling errors						*/

#include "macros.h"
#include "defs.h"
#include "netlab.h"

/*==================================================================*
 *							CLIENT									*
 *==================================================================*/
struct sockaddr_in * initTCPsocket_client(short int port, int *tcpsd){

	struct sockaddr_in *server;
	
	server = malloc(sizeof(struct sockaddr_in ));
	memset(server, 0 , sizeof(struct sockaddr_in ));
	
	/* Create a new socket on the client side to achieve	*
	 * communication with server							*/
	SOCKET_ON_ERROR_FAIL( *tcpsd, AF_INET, SOCK_STREAM, 0)
	
	/* Initialize the connection protocol					*/
	server->sin_family = AF_INET;
	server->sin_port = htons(port);

	/* Connect to server on the socket descriptor got before*/
	CONNECT_ON_ERROR_FAIL(*tcpsd,  (struct sockaddr *)server,sizeof(struct sockaddr_in))

	return server;
}

/*==================================================================*
 *							SERVER									*
 *==================================================================*/
struct sockaddr_in * initTCPsocket_server(short int port, int *ntcpsd){
	
	int tcpsd, opt = 1;
	struct sockaddr_in *client, *nclient;
	
	client = malloc(sizeof(struct sockaddr_in));
	memset(client, 0, sizeof(struct sockaddr_in));

	/* Create a new socket on the server side to achieve	*
	 * communication with clients							*/
	SOCKET_ON_ERROR_FAIL( tcpsd, AF_INET, SOCK_STREAM, 0)
	
	/* Set SO_REUSEADDR as an option for the socket 		*
	 * descriptor created above to force reuse of the socket*
	 * descriptor closed in previous executions				*/
	if(setsockopt(tcpsd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))<0){
		perror("setsockopt");
	}
	
	/* Fill in clients' information							*/
	client->sin_family = AF_INET;
	client->sin_addr.s_addr = INADDR_ANY;
	client->sin_port = htons(port);

	socklen_t clen = sizeof(struct sockaddr_in);
	/* Bind new socket descriptor, and set the socket to 	*
	 * listening state so as to accept new connections		*/
	BIND_ON_ERROR_FAIL(tcpsd,(struct sockaddr*) client, sizeof(struct sockaddr_in))
	LISTEN_ON_ERROR_FAIL(tcpsd, CLIENTQ)
	printf("\n\tListening....Waiting on connections.\n");
	ACCEPT_ON_ERROR_FAIL(*ntcpsd,tcpsd, (struct sockaddr *) &nclient, &clen)
	printf("New client connected to TCP socket.\n");
	
	close(tcpsd);
	
	return client;
}
