#include <stdio.h>		/* Standard input/output library		*/
#include <stdlib.h>		/* General purpose standard library 	*/
#include <string.h>
#include <unistd.h>		/*	prototypes for the string and buffer*
 						 * 				manipulation routines 	*/
#include <sys/socket.h> /* Socket library (socklen_t)			*/
#include <arpa/inet.h> 	/* Definitions for internet operations	*/
#include <errno.h>		/* Handling errors						*/

#include "macros.h"
#include "defs.h"
#include "netlab.h"

/*==================================================================*
 *							CLIENT									*
 *==================================================================*/
void validateUDPknocking_client(int udpsd, int udp_port, struct sockaddr_in *server, socklen_t slen){
	
	char *pong;

	pong = malloc(sizeof(PONG));
	memset(pong, 0, sizeof(PONG));

	/* Send ping message to server and Receive pong message from server	*/
	SENDTO_ON_ERROR_FAIL(udpsd, PING, sizeof(PING), 0,(struct sockaddr *) server, slen)
	RECVFROM_ON_ERROR_FAIL(udpsd, pong, sizeof(PONG), 0,(struct sockaddr *) server, &slen)
	
	printf("\tServer:%s|{port}: %d\n", pong, udp_port);
	close(udpsd);	
}

/*==================================================================*
 *							SERVER									*
 *==================================================================*/
void validateUDPknocking_server(int udpsd, int udp_port, struct sockaddr_in *client, socklen_t clen){

	char *ping;

	ping = malloc(sizeof(PING));
	memset(ping, 0, sizeof(PING));

	/* Receive ping message from client and send pong message to client	*/
	RECVFROM_ON_ERROR_FAIL(udpsd, ping, sizeof(PING), 0,  (struct sockaddr *) client, &clen)
	usleep(200); // This to avoid asynchronous executions on zenon and diogenis
				 // We don't have need of this on Linux (gcc 4.4)
	SENDTO_ON_ERROR_FAIL(udpsd, PONG, sizeof(PONG), 0, (struct sockaddr *) client, clen)

	printf("\tClient:%s|{port}: %d\n", ping, udp_port);
	close(udpsd);
}
