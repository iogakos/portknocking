#include <stdio.h>		/* Standard input/output library		*/
#include <stdlib.h>		/* General purpose standard library 	*/
#include <string.h>
#include <unistd.h>		/*	prototypes for the string and buffer*
 						 * 				manipulation routines 	*/
#include <sys/socket.h> /* Socket library (socklen_t)			*/
#include <arpa/inet.h> 	/* Definitions for internet operations	*/
#include <errno.h>		/* Handling errors						*/

#include "macros.h"
#include "defs.h"
#include "netlab.h"

/*==================================================================*
 *							CLIENT									*
 *==================================================================*/
void validateTCPknocking_client(int tcpsd){
	
	char *welcome;

	welcome = malloc(sizeof(WELCOME));
	memset(welcome, 0, sizeof(WELCOME));

	/* Send 'ping' message to server and Receive 'Welcome' message from server.*/
	SEND_ON_ERROR_FAIL(tcpsd, PING, sizeof(PING), 0)
	RECV_ON_ERROR_FAIL(tcpsd, welcome, sizeof(WELCOME), 0)
	
	printf("\tClient: Received '%s' message from Server.\n",welcome);
	close(tcpsd);
}

/*==================================================================*
 *							SERVER									*
 *==================================================================*/
void validateTCPknocking_server(int tcpsd){
	
	char *ping;

	ping = malloc(sizeof(PING));
	memset(ping, 0, sizeof(PING));
	
	/* Receive ping message from client and send welcome message to client		*/
	RECV_ON_ERROR_FAIL(tcpsd, ping, sizeof(PING), 0)
	SEND_ON_ERROR_FAIL(tcpsd, WELCOME, sizeof(WELCOME), 0)

	printf("\tServer: Received '%s' message from Client.\n",ping);
	close(tcpsd);
}
