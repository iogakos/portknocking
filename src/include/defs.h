/* Our team's port range	*/
#define MINPORT 9570
#define MAXPORT	9579
#define AVAILUDPPORTS 9
#define AVAILTCPPORTS 1
#define MINARGS_CLIENT 4
#define MINARGS_SERVER 6
#define MAXARGS 11
#define MAXCLIENTS 10
#define CLIENTQ MAXCLIENTS

#define OK 0
#define PING "ping"
#define PONG "pong"
#define WELCOME "welcome"
