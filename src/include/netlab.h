struct sockaddr_in * initUDPsocket_client( short int, int *, char *				);
struct sockaddr_in * initUDPsocket_server( short int, int *						);

struct sockaddr_in * initTCPsocket_client( short int, int *						);
struct sockaddr_in * initTCPsocket_server( short int, int *						);

void validateUDPknocking_client( int, int, struct sockaddr_in *, socklen_t		);
void validateUDPknocking_server( int, int, struct sockaddr_in *, socklen_t		);

void validateTCPknocking_client( int											);
void validateTCPknocking_server( int											);
