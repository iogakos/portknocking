/* Macro definitions including basic implementation routines	*
 * such as creation of a new socket, binding a socket descriptor*
 * set socket descriptor to listening state, accepting new 		*
 * connections, connect to socket descriptors and routines to 	*
 * send and receive buffers on UDP/TCP sockets					*
 * Also handling any error occured when executing these 		*
 * functions													*/

#define SENDTO_ON_ERROR_FAIL( A, B, C, D, E, F) \
	if( sendto( A, B, C, D, E, F) < 0){ \
		printf("Error sending to UDP socket:%d\n", errno); \
		perror("Sendto"); \
		exit(1); \
	}

#define RECVFROM_ON_ERROR_FAIL( A, B, C, D, E, F) \
	if( recvfrom( A, B, C, D, E, F)<0){ \
		printf("Error receiving from UDP socket:%d\n", errno); \
		perror("Recvfrom"); \
		exit(1); \
	}

#define RECV_ON_ERROR_FAIL( A, B, C, D) \
	if (recv( A, B, C, D) < 0 ){ \
		printf("Error receiving from TCP socket:%d\n", errno); \
		perror("Recv"); \
		exit(1); \
	}

#define SEND_ON_ERROR_FAIL( A, B, C, D) \
	if (send( A, B, C, D) < 0 ){ \
		printf("Error sending to TCP socket:%d\n", errno); \
		perror("Send"); \
		exit(1); \
	}
#define SOCKET_ON_ERROR_FAIL( A, B, C, D) \
	if( ( A = socket( B, C, D) ) < 0){ \
		printf("Socket create failure:%d\n", errno); \
		perror("Server:socket()"); \
		exit(1); \
	}
	
#define BIND_ON_ERROR_FAIL( A, B, C) \
	if( bind( A, B, C) < 0 ){ \
		printf("Server Bind failure:%d\n",errno); \
		perror("Bind"); \
	}

#define LISTEN_ON_ERROR_FAIL( A, B) \
	if( listen( A, B) < 0){ \
		printf("Error accepting new connection:%d\n",errno); \
		perror("Accept"); \
	}

#define ACCEPT_ON_ERROR_FAIL( A, B, C, D) \
	if(( A = accept( B, C, D) ) < 0){ \
		printf("Error accepting new connection:%d\n",errno); \
		perror("Accept"); \
	}

#define CONNECT_ON_ERROR_FAIL( A, B, C) \
	if ( connect( A, B, C) < 0){ \
		printf("Error connecting to socket:%d\n",errno); \
		perror("Connect"); \
	}
